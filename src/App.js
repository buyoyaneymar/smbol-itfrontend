import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { excuseStore } from "./app/redux-store/excuseSlice";
import { BrowserRouter } from "react-router-dom";

import "./App.css";
import { getAllExcuses } from "./app/api/backend/excuse";
import Routes from "./app/routes/Routes";
import Modal from "./app/components/Modal";

function App() {
  const dispatch = useDispatch();

  /* A hook that is used to fetch data from the backend. */
  useEffect(() => {
    /* A function that is fetching data from the backend. */
    getAllExcuses().then((res) => {
      /* Dispatching the data to the redux store. */
      dispatch(excuseStore(res.data));
    });
  }, []);

  return (
    <div className="ctn">
      <BrowserRouter>
        <Routes />
        <Modal />
      </BrowserRouter>
    </div>
  );
}

export default App;
