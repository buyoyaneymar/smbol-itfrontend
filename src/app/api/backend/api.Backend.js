import  axios from "axios";

/* Creating a new instance of axios with a baseURL. */
const apiBackEnd = axios.create({
    baseURL:"http://localhost:8080/api/",
});
export default apiBackEnd;