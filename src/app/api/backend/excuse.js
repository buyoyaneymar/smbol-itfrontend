import apiBackEnd from "./api.Backend";

/**
 * This function returns a promise that will resolve to an array of excuses.
 * @returns A promise
 */
export function getAllExcuses(){
    return apiBackEnd.get("excuses");
}
/**
 * This function will take the values from the form and send them to the apiBackEnd.post function,
 * which will then send them to the server.
 * @param values - {
 * @returns the apiBackEnd.post method.
 */
export function createNewExcuse(values){
    return apiBackEnd.post("excuses",values);
}