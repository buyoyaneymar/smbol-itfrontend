import React from "react";
import { Route, Routes as RoutesContainer, Navigate } from "react-router-dom";

import CreateExcuse from "../views/CreateExcuse";
import Home from "../views/Home";
import ImLost from "../views/ImLost";
import MessageByHttpCode from "../views/MessageByHttpCode";
import NotFound from "../views/NotFound";

const Routes = () => {
  return (
    <RoutesContainer>
      <Route path={"/"} element={<Home />} />
      <Route path={"/lost"} element={<ImLost />} />
      <Route path={"/:http_code"} element={<MessageByHttpCode />} />
      <Route path={"/create"} element={<CreateExcuse />} />
      <Route path={"/notfound"} element={<NotFound />} />
      <Route path={"*"} element={<Navigate to="/notfound" replace />} />
    </RoutesContainer>
  );
};

export default Routes;
