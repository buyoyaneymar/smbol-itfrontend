import React, { useState } from "react";
import CreateExcuse from "../views/CreateExcuse";

export default function Modal() {
  const [modal, setModal] = useState(false);

  const toggleModal = () => {
    setModal(!modal);
  };

  if (modal) {
    document.body.classList.add("active-modal");
  } else {
    document.body.classList.remove("active-modal");
  }

  return (
    <div className="modal">
      <button onClick={toggleModal} className="btn-modal">
        Creer une escuse
      </button>

      {modal && <CreateExcuse toggleModal={toggleModal} />}
    </div>
  );
}
