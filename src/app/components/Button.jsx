import React from "react";
import { useSelector } from "react-redux";

const Button = ({ setMessage }) => {
  const excuses = useSelector((state) => state.excuse.excuses);
  const max = excuses.length - 1;

  const handleMessage = () => {
    let min = 0;
    const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
    setMessage(excuses[randomNumber].message);
  };

  return (
    <div className="">
      <button className="btn" onClick={() => handleMessage()}>
        Button
      </button>
    </div>
  );
};

export default Button;
