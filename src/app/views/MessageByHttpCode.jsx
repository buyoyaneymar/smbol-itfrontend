import React from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";

const MessageByHttpCode = () => {
  const excuses = useSelector((state) => state.excuse.excuses);
  const params = useParams();

  return (
    <>
      {
        excuses?.find((element) => element.http_code == params.http_code)
          ?.message
      }
    </>
  );
};

export default MessageByHttpCode;
