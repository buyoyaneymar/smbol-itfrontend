import React from "react";

const Affichage = ({ message }) => {
  return (
    <div className="affiche">
      <h3 className="text-white">{message}</h3>
    </div>
  );
};

export default Affichage;
