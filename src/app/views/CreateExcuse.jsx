import React from 'react';
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
// import { CreateExcuseForm } from '../components/CreateExcuseForm'

import { createNewExcuse } from "../api/backend/excuse";
const CreateExcuse = ({toggleModal}) => {
  const navigate = useNavigate();


  const validationSchema = Yup.object().shape({
    http_code: Yup.number()
      .min(1, "trop petit")
      .max(5000, "trop long!")
      .positive()
      .required("Ce champ est obligatoire"),

    tag: Yup.string()
      .min(8, "trop petit")
      .max(200, "trop long!")
      .required("Ce champ est obligatoire"),

    message: Yup.string()
      .min(10, "trop petit")
      .max(5000, "trop long!")
      .required("Ce champ est obligatoire"),
  });
  const formik = useFormik({
    initialValues: {
      http_code: "",
      tag: "",
      message: "",
    },
    onSubmit: (values) => {
      createNewExcuse(values)
        .then((res) => {
          toggleModal();
  

        })
        .catch((error) => {
          if (error.response.status === 500) {
            alert(error.response.data.errorMessage);
          
          }
          toggleModal();
       
        });
    },
  });
  
  return (
    <form
      onSubmit={formik.handleSubmit}
      validationSchema={validationSchema}
      className="form overlay"
    >
      <div className="inputcontenaire">
        <h3>Ajouter une nouvelle escuse</h3>
        <div className="modalspacing">
          {" "}
          <label htmlFor="http_code">http_cod</label>
          <input
            id="http_code"
            name="http_code"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.http_code}
          />
        </div>

        <div className="modalspacing">
          {" "}
          <label htmlFor="tag" className="tag">
            tag
          </label>
          <input
            id="tag"
            name="tag"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.tag}
          />
        </div>

        <div className="modalspacing">
          {" "}
          <label htmlFor="message">message</label>
          <input
            id="message"
            name="message"
            type="message"
            onChange={formik.handleChange}
            value={formik.values.message}
          />
        </div>
        <div className="modalspacing">
          <button type="submit" className="">
            Submit
          </button>
          <span>
            <button onClick={() => toggleModal()} className="cancelbtn">
              Annuler
            </button>
          </span>
        </div>
      </div>
    </form>
  );
}

export default CreateExcuse