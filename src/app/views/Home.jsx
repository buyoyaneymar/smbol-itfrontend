import React from "react";
import Button from "../components/Button";
import Affichage from "./Affichage";
import { useState } from "react";

const Home = () => {
  const [message, setMessage] = useState("Off By Too Many To Count Error");

  return (
    <div className="app">
      <div className="containe titre">
        <div className="centered-element">
          <h1 className="child">Ma super application</h1>
        </div>
      </div>
      <div className="containe message">
        <div className="centered-element">
          <div className="child">
            <Affichage message={message} />
          </div>
        </div>
      </div>
      <div className="containe but">
        <div className="centered-element">
          <div className="child btn">
            {" "}
            <Button setMessage={setMessage} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
