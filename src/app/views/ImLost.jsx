import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const ImLost = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const timer = setTimeout(() => {
      navigate("/");
    }, 5000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <div>
      <p>This page will redirect to in 5 minutes...</p>
    </div>
  );
};

export default ImLost;
