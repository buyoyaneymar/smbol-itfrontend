import { createSlice } from "@reduxjs/toolkit";

/* Setting the initial state of the reducer. */
const initialState = {
  excuses: [],
};

/* Creating a slice of the redux store. */
export const excuseSlice = createSlice({
  name: "excuse",
  initialState,
  reducers: {
    excuseStore: (state, action) => {
      state.excuses = action.payload;
    },
  },
});

/* Exporting the reducer and the action. */
export const { excuseStore } = excuseSlice.actions;
export default excuseSlice.reducer;
