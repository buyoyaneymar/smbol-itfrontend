import { configureStore } from "@reduxjs/toolkit";
import excuseReducer from "./excuseSlice";

/* Creating a store with the reducer. */
export const store = configureStore({
  reducer: {
    excuse: excuseReducer,
  },
});
